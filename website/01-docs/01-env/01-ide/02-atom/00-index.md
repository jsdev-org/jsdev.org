---
layout: page
title: Atom (IDE)
permalink: /env/ide/atom/
---

# Atom (IDE)

[Install Atom on Ubuntu 14.04](/env/ide/atom/install-atom-on-ubuntu-14-04/)

[Atom (IDE) How to Make brackets more visible](/env/ide/atom/how-to-make-brackets-more-visible/)

[Atom (IDE) Plugins](/env/ide/atom/plugins/)
