---
layout: page
title: Most Interesting Video Courses for modern javascript develpment
permalink: /must-have/
---

# Most Interesting Video Courses for modern javascript develpment

I think next courses are very interesting and important to study. And everyone should watch them.


* [Udemy, Andrew Mead] The Complete Node.js Developer Course (3rd Edition) [2019, ENG]
* [Udemy, Stephen Grider] Typescript: The Complete Developer's Guide [2019, ENG] (Very important course for study how to server side development with node.js)
* [Udemy, Brad Traversy] MERN Stack Front To Back: Full Stack React, Redux & Node.js [2019, ENG]
* [Udemy, Stephen Grider] Docker and Kubernetes: The Complete Guide [2018, ENG]

<br/>

Possibe, later i will watch next videos: 

* [Udemy, Andrew Mead] The Modern GraphQL Bootcamp (Advanced Node.js) [2018, ENG]
* [Udemy, Stephen Grider] Advanced React and Redux: 2018 Edition [Udemy, ENG, 2018]
* [Udemy, Stephen Grider] Server Side Rendering with React and Redux [Udemy, ENG, 2017]
