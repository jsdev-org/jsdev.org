---
layout: page
title: PostgreSQL
permalink: /databases/postgresql/
---

<br/>

# PostgreSQL

<br/>

### Cloud providers with free plans

- heroku
- elephantsql.com (20 MB database)


<br/>

### IDE for PostgreSQL

- pgweb-demo.herokuapp.com (web console, free)
- pgAdmin
- datagrip (IntelliJ, paid)

<br/>

### Basic example

https://github.com/marley-nodejs/ReactJS-and-PostgreSQL-Full-Stack-Application
