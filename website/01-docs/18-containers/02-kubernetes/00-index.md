---
layout: page
title: Kubernetes
permalink: /containers/kubernetes/
---

# Kubernetes


<br/>

### Best materials to study kubernetes from scratch

Especially if you want to work with local kubernetes cluster

<br/>

<div align="center">
    <iframe width="853" height="480" src="https://www.youtube.com/embed/YzaYqxW0wGs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<br/>

### [Run local kubernetes cluster](/containers/kubernetes/install/)

<br/>

### [[Stephen Grider] Docker and Kubernetes: The Complete Guide [2018, ENG]](https://github.com/marley-nodejs/Docker-and-Kubernetes-The-Complete-Guide)


+

https://github.com/StephenGrider/docker-react