---
layout: page
title: JavaScript Development Environment
permalink: /env/
---

# JavaScript Development Environment

<br/>

### About My Environment

I'm working on ubuntu linux 14.04 LTS with Docker and my favorite IDE for development is Atom with some plugins. No more anything need for comfort development.

<br/>

<a href="/env/docker/">Docker</a> || <a href="/ide/">IDE</a> || <a href="/env/git/">Git</a> 

<br/>

### How to Disable caching in Chrome Developer Tools

Developer Tools (Ctrl + Shift + I) > Menu icon (the 3 vertical dots) > Settings > Preferences > scroll down the list to “Network section” and you’ll see the “Disable cache” while DevTools is open

<br/>

### Setting up a Minimal, Yet Useful JavaScript Dev Environment

https://dev.to/corgibytes/setting-up-a-minimal-yet-useful-javascript-dev-environment

<br/>

### React and Flux: Migrating to ES6 with Babel and ESLint

https://medium.com/front-end-developers/react-and-flux-migrating-to-es6-with-babel-and-eslint-6390cf4fd878#.kt4kkx7rb


<br/>

## Linting

<a href="/linting/">Linting</a>

<br/>
<hr/>

## Browser Sync

<a href="https://github.com/BrowserSync/browser-sync" rel="nofollow">BrowserSync</a> || LiveReload


<br/>
<hr/>

## Useful browser plugins

postman || firebug || Scratchpad