---
layout: page
title: Export to Excel, Word, Pdf
permalink: /export/
---

<br/>


# Export to Excel, Word, Pdf

<br/>

### PDF

<ul>
    <li><a href="/export/pdf/pdfmake/">pdfmake</a></li>
    <li><a href="https://github.com/MrRio/jsPDF">jsPDF</a></li>
    <li>pdfjs</li>
</ul>


<br/>

### Excel

<ul>
    <li><a href="/export/filesaver/">FileSaver.js</a> (Excel, may be something other)</li>
</ul>
