---
layout: page
title: Fonts
permalink: /fonts/
---

<br/>

# Fonts


https://fonts.google.com/specimen/Roboto

https://fonts.googleapis.com/css?family=Roboto

Understanding Web Fonts in HTML  
https://www.youtube.com/watch?v=VZZ6E1M2kH0


<br/>

### How to Load Web Fonts in 2019  

https://www.youtube.com/watch?v=s-G1m23Emlk