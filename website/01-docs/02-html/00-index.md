---
layout: page
title: HTML CSS etc...
permalink: /html/
---

<br/>

# HTML CSS etc...

<a href="/html/templates/">HTML Templates</a>

<br/>

## Icons:

useiconic.com/open

loading.io/icon/

Icon8 — https://icons8.com/
IconMonstr — https://iconmonstr.com/
Flaticon — https://www.flaticon.com/
IconSearch — http://iconsearch.ru/
ThemeUI — https://themeui.net/free-icons/
Toicon — www.toicon.com/series
TheNounProject — https://thenounproject.com
Fontisto — https://www.fontisto.com/icons
Iconfinder — https://www.iconfinder.com/

<br/>

## Fonts:

<a href="/fonts/">Fonts</a>

<br/>

## CSS preprocessors

less || <a href="/css/preprocessors/sass/">sass</a> ||
<a href="/css/preprocessors/stylus/">stylus</a>

<br/>
<hr/>

## Graphs, Charts, Diagrams

<a href="/charts/">Graphs, Charts, Diagrams</a>

<br/>
<hr/>

## JSON

<a href="/json/">Json</a> || <a href="http://www.jsoneditoronline.org/" rel="nofollow">Json parser</a>
