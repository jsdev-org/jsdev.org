---
layout: page
title: Redux
permalink: /frontend/react/redux/
---

# Redux


    $ npm install --save redux redux-thunk react-redux



### Redux Crash Course With React

<div align="center">
    
    <iframe width="853" height="480" src="https://www.youtube.com/embed/93p3LxR9xfM?start=22" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

</div>

<br/>

src:<br/>

https://github.com/bradtraversy/redux_crash_course
