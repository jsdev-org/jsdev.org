---
layout: page
title: Site Map
permalink: /sitemap/
---

# Site Map

<br/>

## IDE:

<a href="/ide/">IDE</a>

<br/>

## Environment:

<a href="/env/">Environment</a>

<br/>

## HTML CSS Fonts etc...:

<a href="/html/">HTML CSS Fonts etc...</a>

<br/>
<hr/>

## Lang

<a href="/lang/">Lang</a>

<br/>
<hr/>

## Client Side JavaScript

<a href="/frontend/">Frontend</a>

<br/>
<hr/>

## Server Side JavaScript

<a href="/backend/">Backend</a>

<br/>

## Cross-Platform Desktop Apps:

Electron

<br/>

## Mobile Development

Ionic || React Native

<br/>

## Tasks Runner | Preprocessors

<a href="/tasks-runner/webpack/">Webpack</a> ||
Grunt || <a href="/tasks-runner/gulp/">Gulp</a>

## Transpiling

<a href="/transpilers/babel/">Babel</a> || TypeScript (by Microsoft) || Elm || CoffeScpipt || Dart (by Google)

<br/>
<hr/>

## Testing

<a href="/testing/">Testing</a>

<br/>

## Dummy

<a href="/dummy/">Dummy</a>

<br/>
<hr/>

## Secure Tunnels to localhost

<a href="/tunnels/ngrok/">ngrok</a>

<br/>
<hr/>

### DataBases

<a href="/databases/">DataBases</a>

<br/>

### GraphQL

<a href="/graphql/">GraphQL</a>

<br/>

### Containers (Docker and Kubernetes)

<a href="/containers/">Containers</a>

<br/>
<hr/>

## Clouds

<a href="/clouds/">Clouds</a>

<br/>
<hr/>

## Code Quality

Sonarqube

<br/>

## Continious Integration

gitlab-ci | travis-ci | circleci | jenkins | bamboo ci

<br/>

## Email Service

SendGrid (commercial)

<br/>

## Export to Excel, Word, Pdf:

<a href="/export/">Export</a>

<br/>

### API DOC GENERATOR:

<a href="https://github.com/lord/slate" rel="nofollow">slate</a>

<br/>

## Commercial Solutions:

<a href="/commercial/qlik/3.x/">Qlik 3.X</a>

<br/>

## Libs:

<a href="http://markapp.io/" rel="nofollow">markapp</a>
