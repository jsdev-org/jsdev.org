---
layout: page
title: JavaScript Open Source Projects - react
permalink: /src/react/
---

# React.js examples

<br/>

### [FrontendMasters] Complete Intro to React v4 && Intermediate React

[Course Notes](https://btholt.github.io/complete-intro-to-react-v4/)

[Code Repository v4](https://github.com/marley-nodejs/complete-intro-to-react-v4)

[Code Repository v3](https://github.com/btholt/complete-intro-to-react)

<br/>

### React & Webpack 4 From Scratch - No CLI

[React & Webpack 4 From S cratch - No CLI](https://bitbucket.org/marley-react/react-webpack-4-from-scratch-no-cli/)

<br/>

### React.js simple example by code

[demo](http://rawgit.com/MoonHighway/learning-react/master/chapter-05/recipes.html)
[code](https://github.com/MoonHighway/learning-react/blob/master/chapter-05/recipes.js)

<br/>

### Some open source react projects;

<br/>

https://bitbucket.org/marley-nodejs/learning-full-stack-javascript-development-mongodb-node-and

<br/>

https://bitbucket.org/marley-react/webformyself-reactjs-from-zero-to-profy/src/master/

<br/>

**Possible deprecated:**

https://bitbucket.org/marley-react/codedojo-react-basics-part1

<br/>

https://bitbucket.org/marley-react/codedojo-react-basics-part2

<br/>

### Star Rating example

https://rawgit.com/MoonHighway/learning-react/master/chapter-06/03-component-state/02-the-star-rating-component.html

https://github.com/MoonHighway/learning-react/blob/master/chapter-06/03-component-state/02-the-star-rating-component.html
