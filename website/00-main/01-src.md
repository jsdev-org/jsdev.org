---
layout: page
title: JavaScript Open Source Projects
permalink: /src/
---

# JavaScript Open Source Projects

<br/>

### Free opensource eshop React + NodeJs + MongoDB!

https://github.com/cezerin2  
https://cezerin.org

// shop
https://cezerin.ru

// admin
https://cezerin.ru/admin

<!-- // discussion (on russian)
https://searchengines.guru/showthread.php?t=1010199 -->

<br/>

### Interesting code samples

http://todomvc.com/

<!-- <br/>

### Public repos with HTML Templates:

<ul>
    <li><a href="https://github.com/marley-html" rel="nofollow">HTML Templates</a></li>
</ul> -->

<br/>

### [React.js]

# React.js examples

<br/>

### [FrontendMasters] Complete Intro to React v4 && Intermediate React

[Course Notes](https://btholt.github.io/complete-intro-to-react-v4/)

[Code Repository v4](https://github.com/marley-nodejs/complete-intro-to-react-v4)

[Code Repository v3](https://github.com/btholt/complete-intro-to-react)

<br/>

### React & Webpack 4 From Scratch - No CLI

[React & Webpack 4 From S cratch - No CLI](https://bitbucket.org/marley-react/react-webpack-4-from-scratch-no-cli/)

<br/>

### React.js simple example by code

[demo](http://rawgit.com/MoonHighway/learning-react/master/chapter-05/recipes.html)
[code](https://github.com/MoonHighway/learning-react/blob/master/chapter-05/recipes.js)

<br/>

### Some open source react projects;

<br/>

https://bitbucket.org/marley-nodejs/learning-full-stack-javascript-development-mongodb-node-and

<br/>

https://bitbucket.org/marley-react/webformyself-reactjs-from-zero-to-profy/src/master/

<br/>

**Possible deprecated:**

https://bitbucket.org/marley-react/codedojo-react-basics-part1

<br/>

https://bitbucket.org/marley-react/codedojo-react-basics-part2

<br/>

### Star Rating example

https://rawgit.com/MoonHighway/learning-react/master/chapter-06/03-component-state/02-the-star-rating-component.html

https://github.com/MoonHighway/learning-react/blob/master/chapter-06/03-component-state/02-the-star-rating-component.html

<br/>

### Node.js

<ul>
    <li><a href="https://bitbucket.org/marley-nodejs/learn-nodejs-by-building-10-projects" rel="nofollow" target="_blank">Learn Nodejs by building 10 projects [2015, ENG]</a></li>
</ul>

<br/>

### RESTFul

<ul>
    <li><a href="https://bitbucket.org/marley-nodejs/restful-angular-nodejs-mongodb" rel="nofollow" target="_blank">Integrating Angular with Node.js RESTful Services</a></li>
</ul>

<br/>

### Websites:

webpack.js.org  
https://github.com/webpack/webpack.js.org

<!-- <ul>
    <li><a href="https://bitbucket.org/marley-nodejs/" rel="nofollow">Node.js</a></li>
    <li><a href="https://github.com/marley-angular" rel="nofollow">Angular.js</a></li>
    <li><a href="https://github.com/marley-react" rel="nofollow">React.js</a></li>
    <li><a href="https://github.com/marley-knockout" rel="nofollow">Knockout.js</a></li>
    <li><a href="https://github.com/marley-meteor" rel="nofollow">Meteor.js</a></li>
    <li><a href="https://github.com/oracle-jet" rel="nofollow">Oracle Jet</a></li>
    <li><a href="https://github.com/marley-js" rel="nofollow">JavaScript + jQuery</a></li>
</ul> -->
