---
layout: page
title: Angular
permalink: /frontend/angular/
---

# Angular cli

    $ npm init -y
    
    -- without --save flag otherwise error
    $ npm install @angular/cli
    $ ng --version
    
    $ ng new angular-hello-world
    $ cd angular-hello-world
    $ ng serve --host 0.0.0.0 --port 8080
    
    
<br/>

### Making a Component

    $ ng generate component hello-world

<br/>

# Angular from github

Start new angular project


    $ cd ~
    $ mkdir myAngular2Project
    $ cd myAngular2Project/
    $ git clone --depth=1 https://github.com/angular/quickstart .
    $ npm install
    $ npm start


http://localhost:3000/


<br/>

### A curated list of helpful material to get started with education on Angular 2
https://bitbucket.org/marley-angular/angular2-education
