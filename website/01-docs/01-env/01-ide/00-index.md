---
layout: page
title: JavaScript IDE
permalink: /ide/
---

# JavaScript IDE

<br/>

**Visual Studio Code (Microsoft)**

<a href="/env/ide/vscode/">more info</a>

<br/>

**Atom (GitHub || Microsoft)**  
<a href="/env/ide/atom/">more info</a>
