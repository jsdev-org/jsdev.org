---
layout: page
title: HTML Templates
permalink: /html/templates/
---

<br/>

# HTML Templates  

    https://github.com/marley-html?tab=repositories  
    https://html5boilerplate.com/  
    https://html5up.net/  
    http://bootstrapious.com/portfolio-themes  
    http://templated.co  
    http://themes.3rdwavemedia.com/
    http://mdbootstrap.com/  


<br/>

# Fonts  

    https://github.com/ryanoasis/nerd-fonts  
    https://github.com/FortAwesome/Font-Awesome


<br/>

# Styles

    http://getbootstrap.com/
    http://foundation.zurb.com/
    http://www.blueprintcss.org/
    http://firezenk.github.io/zimit/
    http://www.99lime.com/
    https://bootswatch.com/
