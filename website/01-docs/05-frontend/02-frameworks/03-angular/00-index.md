---
layout: page
title: Angular
permalink: /frontend/angular/
---


# Angular

<br/>

### Angular (>= 2.x)

[Angular](/frontend/angular/)

<br/>

### Angular.js (1.x)

[Angular](/frontend/angular/angularjs/)
