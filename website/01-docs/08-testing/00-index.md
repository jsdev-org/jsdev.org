---
layout: page
title: Testing
permalink: /testing/
---

# Testing (unit or functional)

Mocha, Expect, Should, Supertest, Chai, Sinon, Karma, Jasmine etc.


<br/>

### Selenium and Selenium Grid



<br/>

### Test Coverage

Istanbul:  
https://github.com/gotwarlost/istanbul


<br/>

### Testing for AngularJS

protractor (functional):  
http://www.protractortest.org/#/



<br/>

### Installing fake JSON Server for API testing with RestSharp

https://www.youtube.com/watch?v=OuZKeuXkMdc