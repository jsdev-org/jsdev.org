---
layout: page
title: GIT
permalink: /env/git/
---

<br/><br/>

# GIT

https://git-scm.com/

<br/>

### Pro Git book

https://git-scm.com/book/


<br/>

### GitBook project

https:/www.gitbook.com


<br/>

### Git Gui Tools

https://desktop.github.com/ [Mac, Windows]

http://www.syntevo.com/smartgit/ [Linux, Mac, Windows] free for Non commecial

https://www.sourcetreeapp.com/ [Mac, Windows]



<br/>

### A collection of .gitignore templates

https://github.com/github/gitignore


