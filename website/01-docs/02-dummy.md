---
layout: page
title: Dummy
permalink: /dummy/
---

<br/>

# Dummy

<br/>

## Dummy Text Generator

<a href="http://www.lipsum.com" rel="nofollow">Lorem Ipsum</a>

<br/>

## Dummy Image Generator

<a href="http://lorempixel.com/400/400" rel="nofollow">Lorem Pixel</a>
<a href="http://via.placeholder.com/100x100?text=avatar" rel="nofollow">placeholder</a>


<br/>

## Dummy Data Generator

<a href="https://generatedata.com" rel="nofollow">CSV, JSON, SQL, XML...</a>

<br/>

## Working REST service

<a href="https://restcountries.eu/rest/v2/all" rel="nofollow">REST Countries</a>

<a href="https://jsonplaceholder.typicode.com/" rel="nofollow">jsonplaceholder</a>