---
layout: page
title: Node.JS
permalink: /backend/nodejs/
---

# Node.JS

<br/>

### Node Version Manager (NVM) installation on Ubuntu 18.04

    -- current version
    https://github.com/creationix/nvm/releases

    -- for node.js installation
    # apt-get update && apt-get install -qq -y vim git curl net-tools
    # apt-get install -y node.js

    -- nvm

    -- installation
    $ curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash

    $ export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

    // install latest LTS
    $ nvm install --lts

or

    -- show possible
    $ nvm ls-remote
    
    // (Latest LTS: Dubnium)
    $ nvm install v10.16.3

    $ nvm use v10.16.3

    -- set default
    $ nvm alias default v10.16.3

    $ node --version
    v10.16.3

    $ nvm install-latest-npm

    $ npm --version
    6.11.3

<br/>

### Node.JS and Docker

<a href="/env/docker/run-container/linux/">Run Docker Container (with node.js)</a>

<br/>

### Node.JS Static Server

<a href="/backend/nodejs/static-server/">Static Server</a>

<br/>

### Check updates for node packages

    $ npm install -g npm-check-updates
    $ ncu -u
    $ npm install

<br/>

### Node.JS EventEmitter

<a href="/backend/nodejs/event-emitter/">EventEmitter</a>

<br/>

### Node.JS Read Write Steams and Pipes

<a href="/backend/nodejs/read-write-stream/">Node.JS Read Write Steams and Pipes</a>

<br/>

### Nodejs Authentication Using JWT and Refresh Token

[Nodejs Authentication Using JWT and Refresh Token](https://codeforgeek.com/2018/03/refresh-token-jwt-nodejs-authentication/)

src:  
https://github.com/marley-nodejs/node-refresh-token

<br/>

### Node.js login, registration and access control using Express and Passport

[Node.js login, registration and access control using Express and Passport](https://bitbucket.org/marley-nodejs/node.js-login-system-with-passport)

<br/>

### Real-Time-Web-Apps-with-Socket.io

[Real-Time-Web-Apps-with-Socket.io](https://bitbucket.org/marley-nodejs/real-time-web-apps-with-socket.io)

<br/>

### Authentication-API-in-node-express-jwt-bcrypt

[Authentication-API-in-node-express-jwt-bcrypt](https://github.com/marley-nodejs/Authentication-API-in-node-express-jwt-bcrypt)

<br/>

### REST nodejs

[REST nodejs](https://github.com/hengkiardo/restcountries)

<br/>

### Node.JS Articles

<a href="https://codeforgeek.com/2016/08/token-based-authentication-using-nodejs-rethinkdb/" rel="nofollow">[YouTube, Brad Traversy] Building token based authentication using NodeJs and RethinkDB</a>  
<a href="https://codeforgeek.com/2016/09/10-things-running-node-js-production/" rel="nofollow">10 things you should do while running Node.js in production</a>

<a href="https://codeforgeek.com/2016/06/node-js-redis-tutorial-building-email-verification-system/" rel="nofollow">Node.js and Redis tutorial – Building Email verification system</a> and <a href="https://codeforgeek.com/2016/06/nodejs-mandrill-integration/" rel="nofollow">here</a>

<a href="https://codeforgeek.com/2016/06/node-js-redis-tutorial-installation-commands/" rel="nofollow">Node.js-and-Redis-tutorial-Installation-and-commands</a>

<a href="https://codeforgeek.com/2016/04/continuous-integration-deployment-jenkins-node-js/" rel="nofollow">Continuous-Integration-and-deployment-with-Jenkins-and-Node.js</a>

<a href="https://codeforgeek.com/2016/03/hosting-node-js-app-to-digitalocean-server/" rel="nofollow">Hosting Node.js app to DigitalOcean Server</a>

<br/>

### Some commands:

    $ npm init -f
    This will automatically accept the defaults.

    The -f stands for force. These flags also work: --yes, -y, --force.

<br/>

    $ npm list -g --depth=0
    /usr/local/lib
    ├── gulp@3.9.1
    ├── npm@3.9.5
    ├── webpack@1.13.1
    └── webpack-dev-server@1.14.1

<br/>

    $ npm list --depth=0
    04_moviefind@1.0.0 /projects/04 MovieFind
    ├── browserify@13.0.1 extraneous
    ├── gulp@3.9.1 extraneous
    ├── react@0.14.8
    ├── react-dom@0.14.8
    ├── reactify@1.1.1 extraneous
    └── vinyl-source-stream@1.1.0 extraneous

<br/>

    $ npm update -g npm
    $ npm update -g bower
    $ npm update -g gulp

<br/>

    $ npm cache clean
    $ bower cache clean

<br/>

<br/>

### Check npm packages

npms.io

<br/>

### Template trends

https://www.npmtrends.com/pug-vs-ejs-vs-handlebars

<br/>

### Email from Codeforgeek

<br/>

We at Codeforgeek have covered almost every aspects of Node.js development. Starting from Setting up the project to deployment using automated tools.

In this email, we wanted to list down popular node.js post which is considered to be best by our audience. Let us know if you like them.

Here is the list.

<a href="https://codeforgeek.com/2015/01/nodejs-mysql-tutorial/" rel="nofollow">Node.js and MySQL tutorial</a>.  
<a href="https://codeforgeek.com/2014/11/file-uploads-using-node-js/" rel="nofollow">File uploads using Node.js</a>.  
<a href="https://codeforgeek.com/2014/09/handle-get-post-request-express-4/" rel="nofollow">Handle get and post request in Express</a>.  
<a href="https://codeforgeek.com/2015/01/render-html-file-expressjs/" rel="nofollow">Render HTML in ExpressJS</a>.  
<a href="https://codeforgeek.com/2014/09/manage-session-using-node-js-express-4/" rel="nofollow">Manage Session using Node.js and Express</a>.  
<a href="https://codeforgeek.com/2016/04/continuous-integration-deployment-jenkins-node-js/" rel="nofollow">Continuous integration and deployment using Jenkins and Node.js</a>.  
<a href="https://codeforgeek.com/2015/09/real-time-notification-system-using-socket-io/" rel="nofollow">Real-time notification system using Socket.io and Node.js</a>.  
<a href="https://codeforgeek.com/2014/07/node-sqlite-tutorial/" rel="nofollow">Node and SQLite tutorial</a>.  
<a href="https://codeforgeek.com/2015/07/using-redis-to-handle-session-in-node-js/" rel="nofollow">Using Redis to handle Session in Node.js</a>.  
<a href="https://codeforgeek.com/2014/07/send-e-mail-node-js/" rel="nofollow">How to send Email using Node.js</a>.  
<a href="https://codeforgeek.com/2015/03/restful-api-node-and-express-4/" rel="nofollow">Building REST API using Nodejs</a>.  
<a href="https://codeforgeek.com/2015/03/real-time-app-socket-io/" rel="nofollow">Real-time newsfeed using Node and Socket</a>.  
<a href="https://codeforgeek.com/2016/01/multiple-file-upload-node-js/" rel="nofollow">Multiple file upload using Nodejs</a>.  
<a href="https://codeforgeek.com/2015/07/unit-testing-nodejs-application-using-mocha/" rel="nofollow">Test API using Nodejs</a>.  
<a href="https://codeforgeek.com/2014/06/express-nodejs-tutorial/" rel="nofollow">Express and Nodejs tutorial for beginners</a>.

The post is sorted by views count by Google analytics hence they are not in order.

Save the email or bookmark those URL's, I am sure it's going to help you a lot.
