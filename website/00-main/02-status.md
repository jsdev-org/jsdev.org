---
layout: page
title: JSDev status
permalink: /status/
---

# JSDev status

Github blocked repos with project. Status not working right.


<br/>

<a href="https://gitter.im/jsdev-org/Lobby" rel="nofollow"><img src="https://badges.gitter.im/jsdev-org/Lobby.svg" alt="jsdev chat room"></a>
<a href="https://travis-ci.org/jsdev-org/jsdev.org" rel="nofollow"><img src="https://travis-ci.org/jsdev-org/jsdev.org.svg?branch=gh-pages" alt="jsdev build status"></a>
<a href="http://isitmaintained.com/project/jsdev-org/jsdev.org" rel="nofollow"><img src="http://isitmaintained.com/badge/open/jsdev-org/jsdev.org.svg" alt="open issue"></a>
