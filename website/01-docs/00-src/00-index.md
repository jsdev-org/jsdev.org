---
layout: page
title: JavaScript Open Source Projects
permalink: /src/
---

# JavaScript Open Source Projects

<br/>

### Free opensource eshop React + NodeJs + MongoDB!

https://github.com/cezerin2  
https://cezerin.org

// shop
https://cezerin.ru

// admin
https://cezerin.ru/admin

<!-- // discussion (on russian)
https://searchengines.guru/showthread.php?t=1010199 -->

<br/>

### Interesting code samples

http://todomvc.com/

<!-- <br/>

### Public repos with HTML Templates:

<ul>
    <li><a href="https://github.com/marley-html" rel="nofollow">HTML Templates</a></li>
</ul> -->

<br/>

### [React.js]

[here](/src/react/)

<br/>

### Node.js

<ul>
    <li><a href="https://bitbucket.org/marley-nodejs/learn-nodejs-by-building-10-projects" rel="nofollow" target="_blank">Learn Nodejs by building 10 projects [2015, ENG]</a></li>
</ul>

<br/>

### RESTFul

<ul>
    <li><a href="https://bitbucket.org/marley-nodejs/restful-angular-nodejs-mongodb" rel="nofollow" target="_blank">Integrating Angular with Node.js RESTful Services</a></li>
</ul>

<br/>

### Websites:

webpack.js.org  
https://github.com/webpack/webpack.js.org

<!-- <ul>
    <li><a href="https://bitbucket.org/marley-nodejs/" rel="nofollow">Node.js</a></li>
    <li><a href="https://github.com/marley-angular" rel="nofollow">Angular.js</a></li>
    <li><a href="https://github.com/marley-react" rel="nofollow">React.js</a></li>
    <li><a href="https://github.com/marley-knockout" rel="nofollow">Knockout.js</a></li>
    <li><a href="https://github.com/marley-meteor" rel="nofollow">Meteor.js</a></li>
    <li><a href="https://github.com/oracle-jet" rel="nofollow">Oracle Jet</a></li>
    <li><a href="https://github.com/marley-js" rel="nofollow">JavaScript + jQuery</a></li>
</ul> -->
