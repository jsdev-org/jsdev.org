---
layout: page
title: DataBases
permalink: /databases/
---

<br/>

# DataBases


<br/>

### Object-relational mapping (ORM)

- Sequelize - https://github.com/marley-nodejs/Introduction-to-Sequelize-ORM-for-Node.js


+ https://sqlitebrowser.org/ 

<br/>

### MongoDB

[MongoDB](/databases/mongodb/)

<br/>

### MySQL

[MySQL](/databases/mysql/)

<br/>

### PostgreSQL

[PostgreSQL](/databases/postgresql/)

<br/>

### Oracle DataBase

[Oracle DataBase](/databases/oracle/)

<br/>

### Elastic Search

[Elastic Search](/databases/elastic-search/)


<br/>

### FireBase

<a href="https://firebase.google.com" rel="nofollow">FireBase</a> (Google)

<br/>

### RethinkDB

[RethinkDB](/databases/rethinkdb/)

<br/>

### Data For Databases

Open Street Maps:  
https://github.com/OSMNames/OSMNames/releases