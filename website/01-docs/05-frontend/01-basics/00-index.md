---
layout: page
title: Basics examples
permalink: /frontend/js/basics/
---


# Basics examples

<br/>

### Star Ratings With JavaScript & Font Awesome

https://www.youtube.com/watch?v=u3rylF3y3og

<br/>
<br/>

<a href="/frontend/js/run-function-by-click-on-button/">Run Function by Click on button</a>

<a href="/frontend/js/dates/">Working with dates</a>
